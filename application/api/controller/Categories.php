<?php
namespace app\api\controller;
use app\base\Controller;
/**
* 
*/
class Categories extends Controller
{
    
    function __construct(){
        parent::__construct();
    }
    public function index(){
        return $this->view->fetch();
    }
}