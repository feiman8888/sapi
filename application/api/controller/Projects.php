<?php
namespace app\api\controller;
use app\base\Controller;
/**
* 
*/
class Projects extends Controller
{
    
    function __construct(){
        parent::__construct();
        $this->oproject = new \app\apiweb\model\Classify();
    }

    public function index(){
        $projects = $this->oproject->where(['pid'=>'0'])->order('addtime asc')->select();
        if(!empty($projects)){
            foreach ($projects as &$project) {
                $project = $project->toArray();
                $categories = $this->oproject->where(['pid'=>$project['id']])->order('addtime asc')->select();
                if(!empty($categories)){
                    foreach ($categories as $category) {
                        $category = $category->toArray();
                        $project['categories'][] = $category;
                    }
                }
            }
        }
        $this->assign('projects',$projects);
        return $this->view->fetch();
    }

    public function getdata(){
        $cat_id = $this->request->post('catid');
        $projects = [];
        if(empty($cat_id)){
            $projects = $this->oproject->where('pid','0')->select();
            if(!empty($projects)){
                foreach ($projects as $key=>&$project) {
                    $project = $project->toArray();
                    if($key ==0){
                        $cat_id = $project['id'];
                    }
                }
            }
        }
        $categories = $this->oproject->where('pid',$cat_id)->select();
        foreach ($categories as &$category) {
            $category = $category->toArray();
        }
        return ajax_return(['categories'=>$categories,'projects'=>$projects]);
    }

    public function get_data($cat_id){
        $projects = [];
        if(empty($cat_id)){
            $projects = $this->oproject->where('pid','0')->select();
            if(!empty($projects)){
                foreach ($projects as $key=>&$project) {
                    $project = $project->toArray();
                    if($key ==0){
                        $cat_id = $project['id'];
                    }
                }
            }
        }
        $categories = $this->oproject->where('pid',$cat_id)->select();
        foreach ($categories as &$category) {
            $category = $category->toArray();
        }
        return $categories;
    }

    public function get(){
        $cat_id = $this->request->post('catid');
        $category = $this->oproject->where('id',$cat_id)->find();
        if($category){
            $category = $category->toArray();
            if($category['pid'] != '0'){
                $project = $this->oproject->where('id',$category['pid'])->find();
                if(!empty($project)){
                    $project = $project->toArray();
                }
                return ajax_return(['category'=>$category,'project'=>$project]);
            }else{
                return ajax_return(['project'=>$category]);
            }
            return ajax_return($category);
        }else{
            return ajax_return([],'读取失败','error',1);
        }
    }

    public function detail(){
        $pid = $this->request->get('pid');
        $project = $this->oproject->where('id',$pid)->find();
        if(empty($project)){
            $this->error('未查询到该项目信息');
        }
        $project = $project->toArray();
        $project['addtime'] = date('Y-m-d H:i:s',$project['addtime']);
        $categories = $this->oproject->where(['pid'=>$project['id']])->order('addtime asc')->select();
        foreach ($categories as &$category) {
            $category = $category->toArray();
            $category['addtime'] = date('Y-m-d H:i:s',$category['addtime']);
        }
        $this->assign('title','项目详情');
        $this->assign('paths',[['name'=>'项目管理']]);
        $this->assign('project',$project);
        $this->assign('categories',$categories);
        return $this->view->fetch();
    }

    public function del_cat(){
        $cat_id = $this->request->post('catid');
        $status = $this->oproject->where(['id'=>$cat_id])->delete();
        if($status){
            return ajax_return([]);
        }else{
            return ajax_return([],'删除失败','error',1);
        }
    }

    public function add(){
        $data = $this->request->post();
        $data = $data['project'];
        $now_time = time();
        $project = [
            'classifyname' => $data['name'],
            'description'  => $data['intro'],
            'creator'      => '1',
            'leader'       => '1',
        ];
        if(isset($data['pid'])&&!empty($data['pid'])){
            $project['pid'] = $data['pid'];
        }else{
            $project['pid'] = '0';
        }
        if(isset($data['id'])&&!empty($data['id'])){
            $project['id'] = $data['id'];
            $status = $this->oproject->update($project);
        }else{
            $project['addtime'] = $now_time;
            $status = $this->oproject->insert($project);
        }
        if($status){
            return ajax_return([]);
        }else{
            return ajax_return([],'保存失败','error',1);
        }
    }
}