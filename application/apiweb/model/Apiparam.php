<?php
namespace app\apiweb\model;
use think\Model;
/**
* 
*/
class Apiparam extends Model
{
    public $apiType = [
        '1' => 'GET',
        '2' => 'POST',
        '3' => 'PUT',
        '4' => 'DELETE',
    ];
}