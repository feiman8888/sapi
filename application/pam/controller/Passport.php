<?php
namespace app\pam\controller;
use think\Controller;
use think\Session;
/**
* 
*/
class Passport extends Controller
{
    
    function __construct(){
        parent::__construct();
        if($this->request->action() != 'logout'){
            if(!empty(Session::get('uname'))){
                $this->redirect('admin/index/index');
            }
        }
        $this->omember = new \app\admin\model\AdminMembers();
        $this->ogroup  = new \app\admin\model\AdminGroups();
        
    }
    //后台登录
    public function login_admin(){
        return $this->view->fetch();
    }
    //前台登录
    public function login_site(){
        return $this->view->fetch();
    }
    //登录验证
    public function check_admin_login(){
        $postData = $this->request->post();
        $captcha = $postData['login']['checkcode'];
        if(!captcha_check($captcha)){
            return ajax_return([],'验证码错误！','error','1');
        };
        $username = $postData['login']['username'];
        $password = create_password($postData['login']['password'],config('init_salt'));
        $groups = $this->ogroup->where(['type'=>'admin','disable'=>'false'])->select();
        $tmp_gids = [];
        foreach ($groups as $group) {
            $group = $group->toArray();
            $tmp_gids[] = $group['group_id'];
        }
        $filter = [
            'group_id'   => ['in',$tmp_gids],
            'login_name' => $username,
            'password'   => $password,
            'disable'    => 'false'
        ];
        $amember  = $this->omember->where($filter)->find();
        if(empty($amember)){
            return ajax_return([],'用户名或密码错误！','error','1');
        }
        $amember = $amember->toArray();
        Session::set('uname', $amember['login_name']);
        return ajax_return([]);
    }

    public function logout(){
        Session::clear();
        $this->redirect('/login.html');
    }
}