<?php
namespace app\admin\model;
use think\Model;
/**
* 
*/
class Admin_nodes extends Model
{
    /*public $level = [
        ['value' => '1','label' => '一级菜单'],
        ['value' => '2','label' => '二级菜单'],
        ['value' => '3','label' => '三级菜单'],
    ];*/

    protected $parent_id;

    public function get_nodes($parent_id = '0',$pathinfo = null){
        $nodes = $this->where(['parent_id'=>$parent_id,'is_use'=>'true','disable'=>'false'])->field('node_id,name,link,icon')->order('order','asc')->select();
        if(!empty($nodes)){
            foreach ($nodes as &$node) {
                $node = $node->toArray();
                if($pathinfo){
                    $node['active'] = ($pathinfo==$node['link'])?'true':'false';
                }else{
                    $node['active'] = 'false';
                }
                $node['open'] = ($this->parent_id == $node['node_id'])?'true':'false';
                
                $node['link'] = $node['link'] == '#'?:config('root_url').$node['link'];
                $node['child'] = $this->get_nodes($node['node_id'],$pathinfo);
            }
        }
        return $nodes;
    }

    public function get_title($pathinfo = null){
        $node = $this->where(['link'=>$pathinfo,'disable'=>'false','is_use'=>'true'])->find();
        if(!empty($node)){
            $node = $node->toArray();
            $this->parent_id = $node['parent_id'];
            return $node['name'];
        }else{
            return '';
        }
    }

    public function get_parent($parent_id = '0'){
        $node = $this->where('parent_id',$parent_id)->find();
        if(!empty($node)){
            $node = $node->toArray();
            if($node['parent_id'] != '0'){
                $nodes[] = $this->get_parent($parent_id);
            }else{
                $nodes[] = $node['name'];
            }
        }else{
            $nodes[] = $node['name'];
        }
        return $nodes;

    }

    public function get_path($pathinfo = null){
        $node = $this->where(['link'=>$pathinfo,'disable'=>'false','is_use'=>'true'])->find();
        if(!empty($node)){
            $node = $node->toArray();
            if($node['parent_id'] == '0'){
                $nodes = $this->get_parent($node['parent_id']);
            }else{
                $nodes = [];
            }
            return $nodes;
        }else{
            return [];
        }
    }

    public function node_list(){
        $nodes = $this->where('disable','false')->order('create_time','asc')->select();
        if(!empty($nodes)){
            foreach ($nodes as &$node) {
                $node = $node->toArray();
                $node['last_modify'] = date('Y-m-d H:i:s',$node['last_modify']);
                $node['is_use'] = ($node['is_use'] == 'true')?'是':'否';
            }
        }
        return $nodes;
    }
}