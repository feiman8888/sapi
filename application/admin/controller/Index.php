<?php
namespace app\admin\controller;
use app\base\Controller;
/**
* 
*/
class Index extends Controller
{
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        return $this->view->fetch();
    }

}