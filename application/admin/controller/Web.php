<?php
namespace app\admin\controller;
use app\base\Controller;
/**
* 
*/
class Web extends Controller
{
    
    function __construct(){
        parent::__construct();
        $this->osetting = new \app\admin\model\AdminSettings();
    }

    public function index(){
        return $this->view->fetch();
    }

    public function save_theme(){
        $web_theme = $this->request->post('webtheme');
        $web_theme = addcslashes($web_theme,'./-<>');
        $status = $this->osetting->where('key','admin_theme')->update(['value'=>$web_theme]);
        if($status){
            return ajax_return([],'主题设置成功');
        }else{
            return ajax_return([],'主题设置失败','error','1');
        }
    }
    public function save_webmsg(){
        $postData  = $this->request->post();
        $web = $postData['web'];
        $this->osetting->where('key','web_name')->update(['value' => addcslashes($web['name'],'./-<>')]);
        $this->osetting->where('key','web_intro')->update(['value' => addcslashes($web['intro'],'./-<>')]);
        $this->osetting->where('key','web_icpid')->update(['value' => addcslashes($web['icpid'],'./-<>')]);
        /*$obj_files = $this->request->file();
        list($width,$height,$type) = getimagesize($obj_files['logo']->getInfo('tmp_name'));
        if($width != '128' && $height != '128' && $type != '3'){
            return ajax_return([],'请上传128X128的png格式的logo图片','error','1');
        }
        $obj_timg  = new \app\tools\lib\Images();
        $images    = $obj_timg->save_img($obj_files);
        if($images){
            $this->osetting->where('key','web_logo')->update(['value' => $images['logo']]);
        }else{
            return ajax_return([],'logo保存失败','error','2');
        }*/
        return ajax_return();
    }
}